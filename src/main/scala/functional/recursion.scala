package functional

import scala.annotation.tailrec

class recursion {


  val recursion : (Array[Int], Int, (Int, Int)=> Int ) => Int = {
  (arr,acc,combine) =>
  if (arr.isEmpty) acc
  else recursion(arr.tail, combine(arr.head, acc),combine)
  }

  def recursiveCount(x: Array[Int]): Int = {
    @tailrec
    def count(array: Array[Int], acc: Int): Int = {
      if (array.isEmpty) acc
      else count(array.tail, acc + array.head )
    }
    count(x,0)
  }
}
