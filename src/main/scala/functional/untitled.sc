import scala.annotation.tailrec

object Sol {
  def highAndLow(numbers: String): String = {

    val a = numbers.split(" ")
    val b = a.max
    println(a)
    println(b)
    b
  }
  highAndLow("8 3 -5 42 -1 0 0 -9 4 7 4 -4")

}

def recursiveCount(x: Array[Int]): Int = {
  @tailrec
  def count(array: Array[Int], acc: Int): Int = {
    if (array.isEmpty) acc
    else count(array.tail, acc + array.head)
  }

  count(x, 0)
}
recursiveCount(Array(1,2,3,4,5,6))

def quicksort(arr: Array[Int]): Array[Int] = {
  if (arr.length <= 1) arr
  else {
    val pivot = arr(arr.length / 2)
    Array.concat(
      quicksort(arr.filter(_ < pivot)),
      arr.filter(_ == pivot),
      quicksort(arr.filter(_ > pivot))
    )
  }
}
quicksort(Array(2,5,89,574,24,52,1))