package functional

class hof_and_currying {

  //Function Trait
  val add1 = new Function[Int, Int] {
    override def apply(v1: Int): Int = v1 * v1
  }

  val add2 = (i : Int) => i + 2
  //HOF
  //functions that use other functions as arguments
  def HOF (i : Int => Int): Int = {
    i(3)
  }

  def f1(x: Int): Int = {
    x + 2
  }

  def testing(x: Int, y: Int): Int ={
    x+y
  }
  val test: (Int, Int) => Int = (x,y) => x+y
  def f2(y: Int): Int = {
    y+7
  }

  //Function Composition
  f1(f2(4))

  def compositionFunction(x : Int => Int, y: Int => Int) ={
    x(y(2))
  }

  //currying functions
  val noCurry1 : (Int, Int) => Int = (x,y) => x+y
  def noCurry2 (x: Int, y: Int) : Int = {
    x+y
  }

  val curry1 : Int => Int => Int = x => y => x + y
  curry1(1)(2)

  def curry2(x: Int)(y: Int): Int = x+y
  curry2(1)(2)

  //after currying
}
