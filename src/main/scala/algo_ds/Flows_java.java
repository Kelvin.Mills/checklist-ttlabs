package algo_ds;
import java.util.ArrayList;
import java.util.List;

public class Flows_java {

    List<Integer> numList = new ArrayList<>();
    int[] arrString = {1,2,3,4,5,6};
    int number = 7;
    public void ifElseFlow(){
        if(number < 6){
            number += 5;
        }
        else number -= 4;
    }

    public void switchCase(){
        switch (number) {
            case 1 -> System.out.println("1");
            case 2 -> System.out.println("2");
            case 3 -> System.out.println("3");
        }
    }

    public void forLoops(){
        for (int i: arrString){
            System.out.println(i);
        }
        for (int i = 0; i<arrString.length; i+=2){
            System.out.println(i);
        }
    }
}
