package algo_ds

class flows {

  val num = 7

  val arrInt: Array[Int] = Array(1,2,3,4,5,6)
  def ifElseFlow: Int = {
    if (num > 5)  7
    else 6
  }

  def matchFlow(): Unit ={
    num match {
      case 5 => println("yay")
      case 7 => println("no")
      case _ => println("error")
    }
  }

  def forLoops(): Unit = {
    for( i <- arrInt){
      println(i)
    }
  }

}
