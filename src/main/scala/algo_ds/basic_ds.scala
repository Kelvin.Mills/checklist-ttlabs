package algo_ds

import scala.collection.mutable

class basic_ds {
  //different types of data structures in scala

  // Lists
  val list1 : List[Int] = List(1,2,3,4,5,6,8)
  // Sets - have unique elements
  val set1 : mutable.Set[Int] = mutable.Set(1,2,3,4,5,6,7)
  // Array
  val arr1 : Array[Int] = Array(2,4,6,7,8)
  // Map -  key,value pairs
  val map1 : Map[String, Int] = Map("number" -> 1, "number1" -> 2)

}
