package oop.composition_strategy;

public class Multiply implements strategyJava {
    public int compute(int a, int b) {
        return a * b;
    }
}

