package oop.composition_strategy;

public class Test {

    public static void main(String[] args){

        new Compute(new Multiply()).use(2, 3);

    }
}