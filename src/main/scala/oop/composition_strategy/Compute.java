package oop.composition_strategy;

public class Compute {

    private final strategyJava strategy;

    public Compute(strategyJava strategy) {
        this.strategy = strategy;
    }

    public void use(int a, int b) {
        strategy.compute(a,b);
    }
}

