package oop.composition_strategy;

public class Add implements strategyJava {
    public int compute(int a, int b) {
        return a + b;
    }
}

