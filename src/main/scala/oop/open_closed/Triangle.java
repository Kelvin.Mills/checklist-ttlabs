package oop.open_closed;

public class Triangle implements Shape{

    private int base;
    private int height;


    public Triangle(int Base, int Height){
        base = Base;
        height = Height;
    }
    @Override
    public double area(){
        return 0.5 * getBase() * getHeight();
    }
    public int getBase(){
        return base;
    }

    public int getHeight(){
        return height;
    }
}
