package oop.open_closed


//Classes should be open for extension but
//closed for modification


//looking at our AreaShapes class
//what if we have a new shape?
// do we have to modify the AreShapes class?
//That's breaking the Open Closed principle
class OPC {

}
