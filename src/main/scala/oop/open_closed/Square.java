package oop.open_closed;

import oop.SRP.Circle;

public class Square implements Shape {

    private int Length;

    public Square(int length){
        Length = length;
    }

    public int getLength() {
        return Length;
    }

    @Override
    public double area() {
        return Math.pow(getLength(), 2);

    }
}
