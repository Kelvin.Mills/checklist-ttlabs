package oop.open_closed;

public interface Shape {

    double area();
}
