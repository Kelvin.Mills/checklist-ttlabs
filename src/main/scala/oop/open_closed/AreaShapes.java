package oop.open_closed;

import oop.SRP.Circle;
import oop.SRP.Square;

import java.util.List;

//This class calculates the sum of different shapes.
public class AreaShapes {

    public  int sum(List<Shape> shapes){
        int sum = 0;
        for (Shape shape : shapes) {
            sum += shape.area();
        }
        return sum;
    }


}
