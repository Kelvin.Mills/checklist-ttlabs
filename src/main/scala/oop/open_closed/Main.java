package oop.open_closed;

import oop.SRP.ShapesPrinter;
import oop.open_closed.AreaShapes;
import oop.open_closed.Circle;
import oop.open_closed.Square;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AreaShapes areaShapes = new AreaShapes();
        Circle circle = new Circle(10);
        Square square = new Square(10);
        Triangle triangle = new Triangle(5,7);
        ShapesPrinter printer = new ShapesPrinter();
        List<Shape> shapes =  List.of(
                circle,
                square,
                triangle);
        int sum = areaShapes.sum(shapes);
        System.out.println(printer.csv(sum));
        System.out.println(printer.json(sum));
    }
}
