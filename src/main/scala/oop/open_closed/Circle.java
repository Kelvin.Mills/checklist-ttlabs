package oop.open_closed;

import oop.SRP.Square;

public class Circle implements Shape {
    private int Radius;

    @Override
    public double area(){
       return Math.PI * Math.pow(getRadius(), 2);

    }

    public Circle(int radius) {
        Radius = radius;
    }

    public int getRadius(){
        return Radius;
    }


}
