package oop;

import oop.SRP.Circle;
import oop.SRP.Square;

import java.util.List;

//This class calculates the sum of different shapes.
public class AreaShapes {

    public  int sum(List<Object> shapes){
        int sum = 0;
        for (Object shape : shapes) {
            if (shape instanceof Square) {
                sum += Math.pow((((Square) shape).getLength()), 2);
            }
            if (shape instanceof Circle) {
                sum += Math.PI * Math.pow(((Circle) shape).getRadius(), 2);
            }
        }
        return sum;
    }

    //SRP violation - it should only do one thing
//    public String json(List<Object> shapes){
//        return "{sum: %s}".formatted(sum(shapes));
//    }
}
