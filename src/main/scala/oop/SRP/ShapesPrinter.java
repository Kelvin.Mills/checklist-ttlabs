package oop.SRP;

//one responsiblitiy
//to format
public class ShapesPrinter {

    public String json(int  sum){
        return "{sum: %s}".formatted(sum);
    }

    public String csv(int sum) {
        return "shapes_sum, %s".formatted(sum);
    }
}
