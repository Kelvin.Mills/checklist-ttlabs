package oop.SRP;

import oop.AreaShapes;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        AreaShapes areaShapes = new AreaShapes();
        Circle circle = new Circle(10);
        Square square = new Square(10);
        ShapesPrinter printer = new ShapesPrinter();
        List<Object> shapes =  List.of(circle,square);
        int sum = areaShapes.sum(shapes);
        System.out.println(printer.csv(sum));
        System.out.println(printer.json(sum));
    }
}
