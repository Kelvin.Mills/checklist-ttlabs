package oop.liskov;

import oop.open_closed.Shape;

public class NotAShape implements Shape {

    //In this case we
    @Override
    public double area(){
        throw new IllegalStateException("Not a shape");
    }
}
