package oop

class inheritance {
    val name : String = "kelvin";
}

class subclass extends inheritance {

  override val name: String = "overriden name"
}


//just btw
//case class can take arguments,
// so each instance of that case class can be different based on the values of it's arguments.
// A case object on the other hand does not take args in the constructor,
// so there can only be one instance of it (a singleton, like a regular scala object is).