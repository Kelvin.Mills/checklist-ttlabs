import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class JavaOnly {

    public static class Main {
        static List<Integer> setToList(Set<Integer> a){
            return a.stream().toList();
            }

        static int sumOfStringLength(List<String> a){
            List<Integer> b = a.stream().map(String::length).toList();
            return b.stream().mapToInt(i-> i).sum();
        }
        public static void main(String[] args) {


            Set<Integer> a = new HashSet<>();
            a.add(1);
            a.add(2);
            a.add(3);
            a.add(4);
            a.add(5);

            List<Integer> b;

            b = setToList(a);
            System.out.println(b);
        }
    }

}
